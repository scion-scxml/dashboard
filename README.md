# SCION Monitor Dashboard

The monitor utility provides a user interface to view a log of events processed
by the state machine, and display debugging information, such as the datamodel,
inner event queue, and session hierarchy, to visualize how the state changes
over time.

This module contains the dashboard user interface.

Here is a video of the dashboard in action:

[![Link to video](http://img.youtube.com/vi/Pg9tYuJN6BI/0.jpg)](http://www.youtube.com/watch?v=Pg9tYuJN6BI)
